/*
 *  Ammeter. Electric current in a Quick Settings tile.
 *  Copyright (C) 2020-2024  Andrew Nayenko
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.airpost.relan.ammeter;

import android.content.Context;
import android.os.BatteryManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.service.quicksettings.Tile;
import android.service.quicksettings.TileService;
import android.util.Log;

public class AmmeterTileService extends TileService implements Handler.Callback {
    private static final String TAG = "AmmeterTileService";
    private static String mLabelFormat;
    private BatteryManager mBatteryManager;
    private Handler mHandler;

    @Override
    public void onCreate() {
        mLabelFormat = getResources().getString(R.string.label_milliamp);
        mBatteryManager = getSystemService(BatteryManager.class);
        mHandler = new Handler(Looper.getMainLooper(), this);
    }

    @Override
    public void onStartListening() {
        super.onStartListening();

        /* Send a message now (with zero delay) to update the tile */
        mHandler.sendEmptyMessageDelayed(0, 0);
    }

    @Override
    public void onStopListening() {
        super.onStopListening();

        /* Stop updating the tile */
        mHandler.removeMessages(0);
    }

    @Override
    public void onClick()
    {
        super.onClick();

        Log.d(TAG, "Clicked");
    }

    @Override
    public boolean handleMessage(Message msg) {
        int current = mBatteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_NOW);
        Tile tile = getQsTile();

        /*
         * Convert microamperes to milliamperes and change the sign:
         * negative value will indicate that battery is discharging.
         */
        current = -(current + 500) / 1000;
        tile.setLabel(String.format(mLabelFormat, current));
        tile.updateTile();

        mHandler.sendEmptyMessageDelayed(0, 1000 /* 1 second */);
        return true;
    }

}
